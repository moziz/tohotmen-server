const gameElem = document.getElementById("game");
const timerElem = gameElem.getElementsByClassName("timer")[0];
const mapCanvas = gameElem.getElementsByClassName("map-overlay")[0];
const talkerPortraitImageElem = gameElem.getElementsByClassName("talker-portrait")[0];
const talkerMessageTextElem = gameElem.getElementsByClassName("talker-message-text")[0];
const gameIdElement = document.getElementById("game-id");

const showInGameElements = document.getElementsByClassName("show-ingame");
const showSettingUpElements = document.getElementsByClassName("show-settingup");

const startForceButton = document.getElementById("start-force-button");

const textSpeed = 35; // Ship with 35

let ws = null;

const portraitImages = {
  brain: new Image(),
  distraction: new Image(),
  driver: new Image(),
  gun: new Image(),
  lookout: new Image(),
  muscle: new Image(),
  target: new Image(),
  murder: new Image()
};
portraitImages.brain.src        = "img/charactres_brain.png";
portraitImages.distraction.src  = "img/charactres_distraction.png";
portraitImages.driver.src       = "img/charactres_driver.png";
portraitImages.gun.src          = "img/charactres_gun.png";
portraitImages.lookout.src      = "img/charactres_lookout.png";
portraitImages.muscle.src       = "img/charactres_muscle.png";
portraitImages.target.src       = "img/charactres_target.png";
portraitImages.murder.src       = "img/charactres_gun.png";

const sounds = {
  brain: new Audio("audio/TheBrainsAlt_session.ogg"),
  distraction: new Audio("audio/TheDistractionAlt_session.ogg"),
  driver: new Audio("audio/TheDriverAlt_session.ogg"),
  gun: new Audio("audio/TheGunAlt_session.ogg"),
  lookout: new Audio("audio/TheLookoutAlt_session.ogg"),
  muscle: new Audio("audio/TheMuscleAlt_session.ogg"),
  target: new Audio("audio/beebobeeb_session.ogg"),
  murder: new Audio("audio/GunSound_session.ogg")
};

const markerImages = {
  brain: new Image(),
  distraction: new Image(),
  driver: new Image(),
  gun: new Image(),
  lookout: new Image(),
  muscle: new Image(),
  target: new Image()
};
markerImages.brain.src        = "img/mapmarker_brain.png";
markerImages.distraction.src  = "img/mapmarker_distraction.png";
markerImages.driver.src       = "img/mapmarker_driver.png";
markerImages.gun.src          = "img/mapmarker_gun.png";
markerImages.lookout.src      = "img/mapmarker_lookout.png";
markerImages.muscle.src       = "img/mapmarker_muscle.png";
markerImages.target.src       = "img/mapmarker_victim.png";

const cutscenes = [
  { // Intro
    name: "intro",
    markers: {
      target:         { x: 0.5, y: 0.5 },
      brain:          { x: 0.5, y: 0.3 },
      distraction:    { x: 0.1, y: 0.8 },
      driver:         { x: 0.25, y: 0.65 },
      gun:            { x: 0.2, y: 0.3 },
      lookout:        { x: 0.1, y: 0.35 },
      muscle:         { x: 0.7, y: 0.6 }
    },
    dialog: [
     { talker: 'brain', text: "That mouse has meddled long enough with my show business! I have formulated the perfect plan to take her out, and that’s where you come in, my elite team of gangsters!" },
      { talker: 'brain', text: "I've gathered you here for briefing. Lets go through your jobs." },
      { talker: 'brain', text: "Lookout? What do you exactly do?" },
      { talker: 'lookout', text: "I spy the patterns of the target while blending in. I will provide you with information to catch the mouse." },
      { talker: 'brain', text: "Okay, provide the intel to my team and you will get your paycheck." },
      { talker: 'brain', text: "Distraction! You .." },
      { talker: 'distraction', text: "Darling, I know my job. I will charm her with my mesmerizing personality and lead her straight to our trap." },
      { talker: 'brain', text: "This one is most important target yet. You sure you got this?" },
      { talker: 'distraction', text: "No sweat. Mouses are always the easiest prey." },
      { talker: 'brain', text: "Driver? What are you planning?" },
      { talker: 'driver', text: "When the moment comes I pick up our dirty bois." },
      { talker: 'brain', text: "And?" },
      { talker: 'driver', text: "Get out of there full speed. It's a simple job, I got it. Just remember to pay me." },
      { talker: 'brain', text: "Ah, there comes our strong man, The Muscle!" },
      { talker: 'muscle', text: "Yes?" },
      { talker: 'brain', text: "Tell our friends your part Muscle." },
      { talker: 'muscle', text: "I handle guards watching lil mouse. I get gunman out when things go hot." },
      { talker: 'brain', text: "Thank you Muscle." },
      { talker: 'brain', text: "And finally, the vital part of our plot. The Gun, can you squash the rat?" },
      { talker: 'gun', text: "Don't worry. I can handle the dirty deed, as long you make sure everyone else does their job, the mouse is good as dead." },
      { talker: 'brain', text: "Good! Then there is me. I'll be paying you when mouse is dead and no-one rats me out." },
      { talker: 'brain', text: "I'll help you to get stuff moving, but you have to make sure not to leave anything incriminating lying around my hotel room, police surely raid it the moment my old rival is dead.." },
      { talker: 'gun', text: "Enough talk. Lets do this! You all have directions on your screens. BEGIN!" }
    ]
  },

  { // Scene 1
    name: "phase-1-cutscene",
    markers: {
      target:         { x: 0.5, y: 0.5 },
      brain:          { x: 0.5, y: 0.3 },
      distraction:    { x: 0.1, y: 0.8 },
      driver:         { x: 0.25, y: 0.65 },
      gun:            { x: 0.2, y: 0.3 },
      lookout:        { x: 0.1, y: 0.35 },
      muscle:         { x: 0.7, y: 0.6 }
    },
    dialog: [
      { talker: 'lookout', text: "Mouse is smelling the cheese, I repeat mouse is smelling the cheese!" },
      { talker: 'distraction', text: "Your eyes are beautiful as the sea, I could just drown looking at you.." },
      { talker: 'target', text: "Oh you, I’m lucky to meet yo-" },
      { talker: 'murder', text: "RATATATAATAA" },
      { talker: 'distraction', text: "Get down!" },
      { talker: 'murder', text: "RATATATAATAA"},
      { talker: 'gun', text: "Mouse has hit the trap. Pull out!" },
      { talker: 'muscle', text: "Where’s our ride?? We need to get out of here!" },
      { talker: 'lookout', text: "Did she ditch us?" },
      { talker: 'driver', text: "I’m here, get in, lets go!" },
      { talker: 'muscle', text: "Took you long enough, lose the cops!" },
      { talker: 'driver', text: "Full speed!" },
      { talker: 'gun', text: "We made it. The Brains whaddup?" },
      { talker: 'brain', text: "Hit is done. Collect bounty the usual way. I contact you when I need your services again." },
      { talker: 'brain', text: "Thanks to FGJX 2018, you the player, and the accomplices in crime:" },
      { talker: 'gun', text: "Riku Rajaniemi, game client programming." },
      { talker: 'muscle', text: "Janne Hiiskoski, programming big screen client." },
      { talker: 'distraction', text: "Olli Mannevaara, server programming." },
      { talker: 'lookout', text: "Tomi Kokkonen, server programming." },
      { talker: 'driver', text: "Vesa Karjalainen, sound and writing." },
      { talker: 'brain', text: "Salla Hiiskoski, ALL the art." }

    ]
  }
];

const markers = [
  { id: 'target', x: 0.2, y: 0.8 },
  { id: 'brain', x: 0.5, y: 0.1 },
  { id: 'distraction', x: 0.1, y: 0.4 },
  { id: 'driver', x: 0.25, y: 0.9 },
  { id: 'gun', x: 0.2, y: 0.3 },
  { id: 'lookout', x: 0.99, y: 0.35 },
  { id: 'muscle', x: 0.7, y: 0.1 }
];

let gameId = 0;
let lastFrameTime = Date.now();
let gameTime = 0;
let gameStarted = false;
let playCutscene = false;
let cutsceneIndex = 0;
let dialogStartTime = 0;
let dialogIndex = 0;
let dialogPauseEndTime = 0;

function sendAllPlayerConnected() {
  let message = {
    status: "",
    type: "all-players-connected",
    eventType: ""
  };

  ws.send(JSON.stringify(message));
}
startForceButton.onclick = sendAllPlayerConnected;

function getTimerString(minutes) {
  const minutesCount = minutes % 60;
  const hoursCount = Math.floor(minutes / 60);
  return "" + (hoursCount < 10 ? "0" + hoursCount : hoursCount) + ":" + (minutesCount < 10 ? "0" + minutesCount : minutesCount);
}

function getProgressiveString(startTime, charPerSec, text) {
  let out = {
    text: text,
    finished: false
  };

  if (text.length <= 1) {
    out.finished = true;
    out.text = text;
    return out;
  }

  const elapsed = (Date.now() - startTime) / 1000;
  const count = Math.floor(elapsed * charPerSec);

  if (count >= text.length) {
    out.finished = true;
    out.text = text;
    return out;
  }

  const clampedCount = Math.min(text.length, count);

  out.finished = false;
  out.text = text.slice(0, clampedCount);
  return out
}

function updateMarkers(deltaTime) {
  const moveSpeed = 0.3;
  const deltaSeconds = deltaTime / 1000;

  for (let i = 0; i < markers.length; ++i) {
    let marker = markers[i];
    let cutscene = cutscenes[cutsceneIndex];
    let markerTarget = cutscene.markers[marker.id];

    let offsetX = markerTarget.x - marker.x;
    let offsetY = markerTarget.y - marker.y;

    marker.x += moveSpeed * deltaSeconds * offsetX;
    marker.y += moveSpeed * deltaSeconds * offsetY;
  }
}

function updateDialog() {
  let cutscene = cutscenes[cutsceneIndex];

  let progTextResult = getProgressiveString(dialogStartTime, textSpeed, cutscene.dialog[dialogIndex].text);
  talkerMessageTextElem.innerHTML = progTextResult.text;
  talkerPortraitImageElem.src = portraitImages[cutscene.dialog[dialogIndex].talker].src;
  if(progTextResult.finished == false)
  {
    sounds[cutscene.dialog[dialogIndex].talker].play();
  }
  if (progTextResult.finished) {
    if (dialogPauseEndTime == 0)
    {
      let elapsed = Date.now() - dialogStartTime;
      let wait = Math.max(2000, elapsed * 0.75);

      dialogPauseEndTime = Date.now() + wait;
      sounds[cutscene.dialog[dialogIndex].talker].pause();
      sounds[cutscene.dialog[dialogIndex].talker].currentTime = 0;
    }
    else if (dialogPauseEndTime < Date.now())
    {
      ++dialogIndex;
      dialogStartTime = Date.now();
      dialogPauseEndTime = 0;

      if (dialogIndex >= cutscene.dialog.length) {
        // End cutscene
        dialogIndex = 0;
        ++cutsceneIndex;
        playCutscene = false;
        dialogPauseEndTime = 0;

        let message = {
          status: "",
          type: "cutscene-ended",
          eventType: ""
        };
        ws.send(JSON.stringify(message));
      }
    }
  }
}

function updatePanels() {
  for (let i = 0; i < showInGameElements.length; ++i) {
    showInGameElements[i].style.display = gameStarted ? '' : 'none';
  }

  for (let i = 0; i < showSettingUpElements.length; ++i) {
    showSettingUpElements[i].style.display = gameStarted ? 'none' : '';
  }
}

function renderMapOverlay() {
  const ctx = mapCanvas.getContext('2d');

  function pxx(x) {
    return mapCanvas.width * x;
  }

  function pxy(y) {
    return mapCanvas.height * y;
  }

  ctx.clearRect(0, 0, pxx(1), pxy(1));

  for (let i = 0; i < markers.length; ++i)
  {
    let marker = markers[i];
    ctx.drawImage(markerImages[marker.id], pxx(marker.x), pxy(marker.y));
  }
}

function startCutscene(ci) {
  cutsceneIndex = ci;
  playCutscene = true;
  dialogStartTime = Date.now();
  dialogPauseEndTime = 0;
}

function update() {
  const timeNow = Date.now();
  const deltaTime = timeNow - lastFrameTime;

  timerElem.innerHTML = getTimerString(gameTime);

  // Update markers
  if (playCutscene) {
    updateMarkers(deltaTime);
    updateDialog();
  }

  renderMapOverlay();

  lastFrameTime = timeNow;
  window.requestAnimationFrame(update);
}

function startGame() {
  gameStarted = true;

  updatePanels();
  startCutscene(0);

  window.requestAnimationFrame(update);
}

function setupWebSocket(gameId) {
  ws = new WebSocket("ws://" + window.location.host + "/" + gameId);

  ws.addEventListener('open', function (event) {
    let perkele = {
      status: "ok",
      type: "name-player",
      eventData: "BIGSCREENTV_BLACKLISTEDNAME"
    };

    ws.send(JSON.stringify(perkele));
  });

  ws.addEventListener("message", function(event) {
    let obj = null;

    try {
      obj = JSON.parse(event.data);
    } catch (e) {
      console.error("WebSucket-viestin tulkaaminen ei onnistunut:", e, event);
      return;
    }

    console.log("WS:", obj);

    if (obj.type === "new-state" && obj.eventData === "phase-1-cutscene") {
      startGame();
    }
    else if (obj.type === "new-state" && obj.eventData === "phase-1-resolving") {
      startCutscene(1);
    }
    else if (obj.type === "new-state" && obj.eventData === "phase-2-resolving") {
      alert("The end! Your score was " + obj.game.score);
    }
    else if (obj.type === "new-time") {
      gameTime = obj.game.timeLeft;
    }
  });
}

function setupGame() {
  updatePanels();

  let req = new XMLHttpRequest();

  req.addEventListener("load", function(res) {
    gameId = req.responseText;

    gameIdElement.innerHTML = gameId;
    setupWebSocket(gameId);
  });

  req.open("GET", "http://" + window.location.host + "/api/game/new-game");
  req.send();
}

setupGame();
