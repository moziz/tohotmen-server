/**
 * Created by tomikokkonen on 27.1.2018.
 */
import { NextFunction, Request, Response } from "express";
import { Game } from "../models/game.model";

import { BAD_REQUEST, NOT_FOUND, OK } from "http-status-codes";

class JoinController {
  public async index(req: Request, res: Response, next: NextFunction) {
    const { joinCode = 9999} = req.query;

    const game = null;

    //find game and return

    res.status(OK).json(game);
  }
}

export const joinController = new JoinController();
