/**
 * Created by tomikokkonen on 27.1.2018.
 */
import {Router, Request, Response, NextFunction} from "express";
import {webSocketService} from "../web-socket.service";

const router = Router();

router
  .route("/new-game/")
  /**
   * GET join
   */
  .get((req: Request, res: Response, next: NextFunction) => {
    let joinCode = webSocketService.startNewGame();
    res.status(200).send(joinCode);
  });

module.exports = router;