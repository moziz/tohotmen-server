/**
 * Created by tomikokkonen on 27.1.2018.
 */
import { Router } from "express";
import { joinController } from "../controllers/join.controller";
import { joinValidation } from "../validations/join.validation";

const router = Router();

router
  .route("/")
  /**
   * GET join
   */
  .get(joinValidation.index, joinController.index);
module.exports = router;