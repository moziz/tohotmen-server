import bluebird = require("bluebird");
import bodyParser = require("body-parser");
import compression = require("compression");
import cors = require("cors");
import express = require("express");
import validator = require("express-validator");
import {webSocketService} from "./web-socket.service";
import {GameSessionService} from "./game-session.service";
import path = require("path");
import Logger = require("bunyan");
const logger = new Logger({name:"tohotmen-server"});

export class Server {

  public gameSessionService: GameSessionService;

  public async init() {
    const app = await this.configure();
    const httpServer = app.listen(29920);
    logger.info(`Server on port 29920`);
    this.gameSessionService = new GameSessionService();
    webSocketService.init(this.gameSessionService, httpServer);
    webSocketService.startNewGame();
    return app;
  }

  private async configure() {
    const app = express();

    app.use(cors());
    //app.use(helmet());
    app.use(compression());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(validator());

    app.use("/api/join", require("./routes/join.route"));
    app.use("/api/game", require("./routes/game.route"));

    //static
    let pathi = path.join(__dirname, "../../../statustin/");
    logger.info("path to statustin " + pathi);
    app.use("/", express.static(pathi));

    return app;
  }
}
