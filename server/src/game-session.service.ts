import {Game} from "./models/game.model";
import WebSocket = require('ws');
import Logger = require("bunyan");
const logger = new Logger({name:"tohotmen-server"});

export class GameSessionService {
  public sessions: Game[];

  constructor() {
    this.sessions = [];
  }

  public GetGame(joinCode: string) {
    return this.sessions.find(value => value.joinCode == joinCode);
  }

  public playerConnected(joinCode: string, ws: WebSocket) {
    let game = this.GetGame(joinCode);
    if (game == null) {
      return null;
    }
    else {
      //Create new player to that game and return it
      let newPlayer = game.NewPlayerJoins(ws);
      return newPlayer;
    }
  }

  public startNewGame() {
    let newGame = new Game();
    //random joinCode

    let joinCode = 1337;
    while (this.sessions.find(value => value.joinCode === joinCode.toString())) {
      joinCode = Math.floor(Math.random() * 8999) + 1001;
    }
    newGame.joinCode = joinCode.toString();
    this.sessions.push(newGame);
    logger.info("new game started join code: " + joinCode.toString());
    return newGame;
  }

}