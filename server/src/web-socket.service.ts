//import URL = require('url');
import WebSocket = require('ws');
import * as http from 'http';
import {GameSessionService} from "./game-session.service";
import {Game, Player, PlayerMessage} from "./models/game.model";
import Logger = require("bunyan");
const logger = new Logger({name:"tohotmen-server"});
const StringDecoder = require('string_decoder').StringDecoder;

const WebSocketServer = WebSocket.Server;

class WebSocketService {
  private gameSessionService: GameSessionService;

  public init(gameSessionService: GameSessionService, httpServer: http.Server) {
    this.gameSessionService = gameSessionService;
    //Generate new server
    let server = new WebSocketServer({
      server: httpServer
    });

    server.on('connection', (ws: WebSocket, request: http.IncomingMessage) => {
      if (!request || !request.url) {
        ws.send(JSON.stringify({error: "Vammanen url"}));
        ws.close(4001, JSON.stringify({error: "Game not found"}));
        return;
      }

      let joinCode = request.url.substring(1);
      logger.info("connection on url:" + joinCode);
      if (joinCode == null || joinCode === "") {
        ws.send(JSON.stringify({error: "Game not found"}));
        ws.close(4002, JSON.stringify({error: "Game not found"}));
        return;
      }
      let game = this.gameSessionService.GetGame(joinCode);
      let player = this.gameSessionService.playerConnected(joinCode, ws);
      if (player == null || game == null) {
        ws.send(JSON.stringify({error: "Game not found"}));
        ws.close(4003, JSON.stringify({error: "Game not found"}));
        return;
      }

      //use ws to get messages
      ws.on('message', (data: WebSocket.Data) => {
        // logger.info("got message from user", typeof data);
        let stringData = "";
        try {
          if (typeof data == "string") {
            stringData = data.toString();
          }
          else {
            let decoder = new StringDecoder('utf8');
            stringData = decoder.write(data);
          }
          logger.info("Got string data from user: " + stringData);
          let message: PlayerMessage = JSON.parse(stringData);

          //logger.info("Got message from " + player.index + " message: ", message);
          if (message != null && game != null && player != null) {
            game.MessageFromPlayer(player, message);
          }
        }
        catch (error) {
          console.error(error);
          ws.send("json parse failed " + error.message + " stringData: " + stringData);
        }
      });

      ws.on('close', (code: number, reason: string) => {
        logger.info("Player disconnected");
        if(game && player){
          game.PlayerDisconnected(player);
        }
      });

      ws.on('error', (error:any, message:any, type:string, target:WebSocket)=>{
        logger.error("webscocket error",{message:message, error:error});
        if(game && player){
          game.PlayerDisconnected(player);
        }
      });
    });

    server.on('error', (error: Error) => {
      logger.info("WebSocketServer error ", error);
    });

    logger.info("Listening ws-connections at 29920");
    return this;
  }

  public startNewGame() {
    let newGame = this.gameSessionService.startNewGame();
    return newGame.joinCode;
  }
}
export const webSocketService = new WebSocketService();
