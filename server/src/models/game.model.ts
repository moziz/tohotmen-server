import WebSocket = require('ws');
import Logger = require("bunyan");

const logger = new Logger({name: "tohotmen-server"});

const data = {
  roles: ["murhaaja", "kuski", "muskelit", "tarkkailija", "syötti", "aivot"],
  getCards: (role: string): string[] => {
    switch (role) {
      case "murhaaja":
        return [
          "ase",
          "radio",
          "valepuku"
        ];
      case "kuski":
        return [
          "sanomalehti",
          "oljy",
          "valepuku",
          "-viini"
        ];
      case "muskelit":
        return [
          "ase",
          "raha",
          "housut"
        ];
      case "tarkkailija":
        return [
          "radio",
          "sanomalehti",
          "housut"
        ];
      case "syotti":
        return [
          "viini",
          "oljy",
          "ruusukimppu"
        ];
      case "aivot":
        return [
          "-viini",
          "-ase",
          "-valepuku"
        ];
    }
    logger.error("No role: " + role);
    return [];
  },
  cards: [
    "valepuku",
    "oljy",
    "ase",
    "radio",
    "raha",
    "viini",
    "housut",
    "sanomalehti",
    "ruusukimppu"] as string[]

};

let shuffle = (a: any[]) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

let getRandomElement = (a: any[]) => {
  let randomIndex = Math.floor(Math.random() * (a.length));
  if (randomIndex >= a.length) {
    logger.info("getRandomElement got too big index");
    randomIndex = a.length - 1;
  }
  return a[randomIndex];
};

export class Player {
  constructor(ws: WebSocket, index: string) {
    this.SendMessage = (message: GameMessage) => {
      if (ws != null && ws.readyState == WebSocket.OPEN) {
        ws.send(JSON.stringify(message));
      }
    };
    this.GetSocket = () => {
      return ws;
    };
    this.score = 0;
    this.roundScores = [];
    this.cardsInHand = [];
    this.cardOfferLeft = new Card();
    this.cardOfferRight = new Card();
    this.role = "";
    this.missionCards = [];
    this.index = index;
    this.name = "";
  }

  public score: number;
  public roundScores: { round: number, score: number }[];
  public index: string;
  public name: string;
  public cardsInHand: Card[];
  public cardOfferRight: Card;
  public cardOfferLeft: Card;
  public missionCards: string[];
  public role: string;

  public GetSocket: () => WebSocket;
  public SendMessage: (message: GameMessage) => void;
}

export class Card {
  constructor() {
    this.type = "";
    this.index = "";
  }

  type: string;
  index: string;

  toString() {
    return "CARD#" + this.index + " :" + this.type;
  }
}

export class GameMessage {
  status: string;
  type: string;
  eventData: string; // type depententti json
  game: Game;
}

export class PlayerMessage {
  status: string;
  type: string;
  eventData: string; // type depententti json
}

export class Game {

  score: number;
  joinCode: string;
  players: Player[];
  disconnectedPlayers: Player[];
  tvClients: Player[];
  skippingPlayers: string[];
  state: string;
  playerIndexCounter = 1337;
  cardIndexCounter = 123;
  roundNumber: number = 0;
  roundScores: { round: number, score: number }[];
  timeLeft: number = -1;

  //intervalTimer: any;

  constructor() {
    this.players = [];
    this.disconnectedPlayers = [];
    this.roundScores = [];
    this.tvClients = [];
    this.skippingPlayers = [];
    this.state = "waiting-players";
    this.score = 0;
    let intervalTimer = setInterval(this.IntervalFunction.bind(this), 995);
  }

  private IntervalFunction() {
    if (this.timeLeft == -1) {
      return;
    }
    if (this.timeLeft > 0) {
      this.timeLeft--;
    }
    if (this.timeLeft < 1) {
      this.timeLeft = -1;
      this.TimeOut();
    }
    this.BroadcastMessage("time", "new-time", this.timeLeft);
  }

  public NewPlayerJoins(ws: WebSocket) {
    ++this.playerIndexCounter;
    let newPlayer = new Player(ws, this.playerIndexCounter.toString());
    //Add new player
    this.players.push(newPlayer);
    newPlayer.SendMessage({
      status: "joined",
      type: "you-joined",
      eventData: JSON.stringify(newPlayer),
      game: this
    });

    return newPlayer;
  }

  public PlayerDisconnected(player: Player) {
    if (this.tvClients.find(value => value.index == player.index) != null) {
      this.disconnectedPlayers.push(player);
      this.tvClients.splice(this.tvClients.indexOf(player), 1);
    }
    else {
      this.players.splice(this.players.indexOf(player), 1);
      this.disconnectedPlayers.push(player);
      this.BroadcastMessage(
        "normal",
        "player-removed",
        player
      );
    }
  }

  public BroadcastMessage(status: string, type: string, eventData: any) {
    if (eventData == "") {
      eventData = "{}";
    }
    if (typeof eventData !== "string") {
      eventData = JSON.stringify(eventData);
    }
    for (let player of this.players) {
      player.SendMessage({
        status: status,
        type: type,
        eventData: eventData,
        game: this
      });
    }
    for (let tvClient of this.tvClients) {
      tvClient.SendMessage({
        status: status,
        type: type,
        eventData: eventData,
        game: this
      });
    }
  }

  public StartPhaseCutScene(round: string) {
    this.state = "phase-" + round + "-cutscene";
    this.BroadcastMessage("ok", "new-state", "phase-1-cutscene");
  }

  public StartTraidingPhase() {
    this.skippingPlayers = [];
    let numberOfPlayers = this.players.length;
    let roles = ["murhaaja"];
    let rolePool = ["kuski", "muskelit", "tarkkailija", "syötti", "aivot"];
    shuffle(rolePool);

    while (roles.length < numberOfPlayers) {
      if (rolePool.length == 0) {
        rolePool = data.roles.slice();
        shuffle(rolePool);
      }
      let randomRole = rolePool.splice(0, 1);
      roles.push(randomRole[0]);
      logger.info("added role " + randomRole[0]);
    }
    shuffle(roles);

    //hand roles to players
    for (let i in this.players) {
      this.players[i].role = roles[i];
      this.players[i].missionCards = data.getCards(roles[i]);
      logger.info("set role " + roles[i] + " to " + this.players[i].name);
    }

    //make a deck
    let deck: Card[] = [];
    for (let role of roles) {
      let cardTypes: string[] = data.getCards(role);
      for (let cardType of cardTypes) {
        if (cardType.startsWith("-")) {
          let newCard = new Card();
          newCard.type = cardType.substring(1);
          newCard.index = (this.cardIndexCounter++).toString();
          deck.push(newCard);
        }
        else {
          let newCard = new Card();
          newCard.type = cardType;
          newCard.index = (this.cardIndexCounter++).toString();
          deck.push(newCard);
        }
        logger.info("New card from roles", deck);
      }
    }

    while (deck.length < 4 * this.players.length) {
      let cardType = getRandomElement(data.cards);
      let newCard = new Card();
      newCard.type = cardType;
      newCard.index = (this.cardIndexCounter++).toString();
      deck.push(newCard);
    }

    logger.info("deck built ", deck);
    shuffle(deck);
    //Deal cards:
    for (let player of this.players) {
      player.cardOfferLeft = new Card();
      player.cardOfferRight = new Card();
      player.cardsInHand = deck.splice(0, 4);
      logger.info("player " + player.name + " got cards ", player.cardsInHand);
    }

    // start 3minute timer
    this.timeLeft = 2 * 60;

    this.state = "phase-" + this.roundNumber + "-trading";
    this.BroadcastMessage("ok", "new-state", this.state);
  }

  public MessageFromPlayer(sender: Player, message: PlayerMessage) {
    logger.info("Got message from " + sender.index + " message: " + message.type + "=" + message.eventData);
    switch (message.type) {
      case  "all-players-connected": {
        //Start game
        if (this.state === "waiting-players") {
          //start first round
          this.roundNumber = 1;
          this.StartPhaseCutScene("1");
        }
        break;
      }
      case "name-player": {
        sender.name = message.eventData;
        if (sender.name === "") {
          sender.name = "Nimetön";
        }
        if (sender.name === "BIGSCREENTV_BLACKLISTEDNAME") {
          this.players.splice(this.players.indexOf(sender), 1);
          this.tvClients.push(sender);
        }
        else {
          //send info to others
          this.BroadcastMessage(
            "normal",
            "player-joined",
            sender
          );
        }
        break;
      }
      case "player-skip": {
        if (this.skippingPlayers.indexOf(sender.index) == -1) {
          this.skippingPlayers.push(sender.index);
        }
        if (this.skippingPlayers.length >= this.players.length) {
          if (this.state.endsWith("-cutscene")) {
            this.StartTraidingPhase();
          }
          else if (this.state.endsWith("-resolving")) {
            this.roundNumber++;
            this.StartTraidingPhase();
          }
        }
        break;
      }
      case "resolving-ended":
      case "cutscene-ended": {
        if (this.state.endsWith("-cutscene")) {
          this.StartTraidingPhase();
        }
        else if (this.state.endsWith("-resolving")) {
          this.roundNumber++;
          this.StartTraidingPhase();
        }
        break;
      }
      case "trade-offer-left": {
        // create actual card object:
        let cardFromSender = new Card();
        if (message.eventData != "") {
          let cardDataFromSender = JSON.parse(message.eventData);
          cardFromSender.index = cardDataFromSender.index;
          cardFromSender.type = cardDataFromSender.type;
        }
        else {
          logger.info("Remove card from left");
          if (sender.cardOfferLeft.index !== "") {
            logger.info("Card removed from left");
            sender.cardsInHand.push(sender.cardOfferLeft);
            sender.cardOfferLeft = new Card();
          }
          else {
            logger.info("Player had no card on left");
          }
          this.BroadcastMessage("ok", "new-card-offer", "{}");
          return;
        }

        //Set the offer
        let cardFromHand = sender.cardsInHand.find(value => value.index === cardFromSender.index);
        logger.info("Sender had card " + cardFromHand + " in hand");
        if (cardFromHand == null) {
          //had no in hand, check the other offer
          if (sender.cardOfferRight.index === cardFromSender.index) {
            logger.info("moved from right to left");
            cardFromHand = sender.cardOfferLeft;
            sender.cardOfferRight = new Card();
          }
          else {
            logger.info("" + sender.name + " has no card " + message.eventData);
            this.BroadcastMessage("ok", "new-card-offer", "{}");
            return;
          }
        }
        else {
          //remove card from hand
          sender.cardsInHand.splice(sender.cardsInHand.indexOf(cardFromHand), 1);
        }

        //remove old offer
        if (sender.cardOfferLeft.index !== "") {
          logger.info("move card from r-offer to hand " + sender.cardOfferLeft);
          sender.cardsInHand.push(sender.cardOfferLeft);
        }
        //Set offer
        sender.cardOfferLeft = cardFromSender;

        //Tarksita jos vaihtoja tapahtuu
        let target = this.GetLeft(sender);
        if (target.cardOfferRight.index !== "") {
          this.swap(target, sender);
        }
        this.BroadcastMessage("ok", "new-card-offer", "{}");
        break;
      }
      case "trade-offer-right": {
        // create actual card object:
        let cardFromSender = new Card();

        if (message.eventData != "") {
          let cardDataFromSender = JSON.parse(message.eventData);
          cardFromSender.index = cardDataFromSender.index;
          cardFromSender.type = cardDataFromSender.type;
        }
        else {
          logger.info("Remove card from right");
          if (sender.cardOfferRight.index !== "") {
            logger.info("Card removed from right");
            sender.cardsInHand.push(sender.cardOfferRight);
            sender.cardOfferRight = new Card();
          }
          else {
            logger.info("Player had no card on right");
          }
          this.BroadcastMessage("ok", "new-card-offer", "{}");
          return;
        }

        //Set the offer
        let cardInHand = sender.cardsInHand.find(value => value.index === cardFromSender.index);
        logger.info("Player had card " + cardInHand + " in hand");
        if (cardInHand == null) {
          if (sender.cardOfferLeft.index === cardFromSender.index) {
            logger.info("moved from left to right");
            cardInHand = sender.cardOfferLeft;
            sender.cardOfferLeft = new Card();
          }
          else {
            logger.info("" + sender.name + " has no card " + message.eventData);
            return;
          }
        }
        else {
          //remove card from hand
          sender.cardsInHand.splice(sender.cardsInHand.indexOf(cardInHand), 1);
        }

        //remove old offer
        if (sender.cardOfferRight.index !== "") {
          logger.info("move card from r-offer to hand " + sender.cardOfferRight);
          sender.cardsInHand.push(sender.cardOfferRight);
        }
        //Set offer
        sender.cardOfferRight = cardFromSender;

        //Tarksita jos vaihtoja tapahtuu
        let target = this.GetRight(sender);
        if (target.cardOfferLeft.index !== "") {
          this.swap(sender, target);
        }
        this.BroadcastMessage("ok", "new-card-offer", "{}");
        break;
      }
      default: {
        logger.info("No message type of " + message.type);
        sender.SendMessage({
          status: "FAIL!",
          type: "invalid-message-type",
          eventData: message.type,
          game: this
        });
      }
    }
  }

  public swap(left: Player, right: Player) {
    logger.info("Players changing cards");
    let fromLeft = left.cardOfferRight;
    let fromRight = right.cardOfferLeft;
    left.cardOfferRight = new Card();
    right.cardOfferLeft = new Card();
    left.cardsInHand.push(fromRight);
    right.cardsInHand.push(fromLeft);
    this.BroadcastMessage("ok", "card-trade", {
      leftPlayer: left,
      rightPlayer: right,
      fromLeft: fromLeft,
      fromRight: fromRight
    });
  }

  public GetLeft(p: Player) {
    let result = -1;
    for (let i = 0; i < this.players.length; ++i) {
      if (this.players[i].index == p.index) {
        result = i - 1;
        break;
      }
    }
    if (result < 0) {
      result = this.players.length - 1;
    }
    return this.players[result];
  }

  public GetRight(p: Player) {
    let result = -1;
    for (let i = 0; i < this.players.length; ++i) {
      if (this.players[i].index == p.index) {
        result = i + 1;
        break;
      }
    }
    if (result >= this.players.length) {
      result = 0;
    }
    return this.players[result];
  }

  public TimeOut() {
    if (this.state.endsWith("-trading")) {
      //return cards to hand and calculate scores
      let roundScore = 0;
      let rightPercentage = 0;
      for (let player of this.players) {
        if (player.cardOfferLeft.index !== "") {
          player.cardsInHand.push(player.cardOfferLeft);
          player.cardOfferLeft = new Card();
        }
        if (player.cardOfferRight.index !== "") {
          player.cardsInHand.push(player.cardOfferRight);
          player.cardOfferRight = new Card();
        }
        let rightCount = 0;
        for (let mission of player.missionCards) {
          if (mission.startsWith("-")) {
            rightCount++;
            continue;
          }
          let result = player.cardsInHand.find(value => value.type == mission);
          if (result != null) {
            rightCount++;
          }
        }
        rightCount = Math.min(rightCount, 3);
        rightPercentage += rightCount;
        let score = 10 + (rightCount * rightCount) * 10;
        player.score += score;
        player.roundScores.push({round: this.roundNumber, score: score});
        roundScore += score;
        logger.info("player " + player.name + " got " + score + " score ");
      }
      if (this.players.length > 0) {
        rightPercentage = rightPercentage / this.players.length;
      }
      else {
        rightPercentage = 1;
      }
      logger.info("percentage " + rightPercentage);
      roundScore = Math.ceil(roundScore * rightPercentage);
      this.score += roundScore;
      this.roundScores.push({round: this.roundNumber, score: roundScore});
      logger.info("team got " + roundScore);
      this.state = "phase-" + this.roundNumber + "-resolving";
      this.BroadcastMessage("ok", "new-score", "{}");
      this.BroadcastMessage("ok", "new-state", this.state);
    }
    else {
      logger.info("Got timeout without trading phase!");
    }
  }
}
