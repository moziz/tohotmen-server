import {NextFunction, Request, Response} from "express";

import {validator} from "../helpers/validator";

class JoinValidation {
  public index = validator({
    joinCode: {
      in: "query",
      isInt: {
        errorMessage: "Please Provide An Integer Value Less Than 9999",
        options: [{max: 9999}]
      },
      notEmpty: true
    },
    name: {
      in: "query",
      notEmpty: true
    }
  });
}

export const joinValidation = new JoinValidation();
