# TypeMonPress

## Overview

## Getting Started

Clone the repo:
```sh
git clone git@bitbucket.org/moziz/tohotmen-server.git
cd TypeMonPress
```

Install dependencies:
```sh
npm install
```

Start server:
```sh
# Start server
npm start

# Start debug server
npm run debug
```